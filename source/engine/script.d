﻿/// Containers a Parser and all of the AST stuff for the engine's scripting.
module engine.script;

private
{
    import std.algorithm    : canFind;
    import std.range        : isInputRange;
    import std.exception    : enforce;
    import std.format       : format;
    import std.ascii        : isWhite, isDigit;
    import std.file         : readText;
    import std.conv         : to;

    import std.array;
}

/// The type of node.
enum NodeType
{
    /// Room declaration
    Decl_Room,

    /// Hook Declaration
    Decl_Hook,

    /// Function call Declaration
    Decl_Call,

    /// Script declaration
    Decl_Script,

    /// String value
    Value_String,

    /// Number value
    Value_Number,

    /// Call value
    Value_Call,

    /// Hook value
    Value_Hook,

    /// Node of nodes
    Body,

    /// Include node
    Include,

    /// File node
    File,

    /// Comparison node
    Comparison
}

/// The type of a token.
enum TokenType
{
    /// Special characters.
    Operator,

    /// Numeric literals
    Number,

    /// Textual literals.
    String,

    /// Certain kind of text with meaning
    Keyword,

    /// Text that has no predefined meaning.
    Identifier,

    /// End of the code.
    EoF
}

/// Contains info used to debug errors.
private struct DebugInfo
{
    /// What file the debug info was created in.
    string file;

    /// What line the thing was made on.
    size_t line;

    /// How many characters across on the line the thing was made on.
    size_t column;
}

/// A token created by the Parser.
struct Token
{
    /// What type of token it is.
    TokenType   type;

    /// The text that makes up the token
    string      text;

    /// Debug information for the token
    DebugInfo   info;

    /// Convinience method
    void enforceType(TokenType wanted, string wantedFor)
    {
        enforce(wanted == this.type, 
            new ASTException(
                format("For '%s'. Wanted token of type '%s'. Got token of type '%s'. Token = %s", 
                    wantedFor, wanted, this.type, this), 
                this.info)
            );
    }

    void enforceAll(TokenType wantedType, string wantedText)
    {
        enforce(wantedText == this.text && wantedType == this.type,
            new ASTException(
                format("Wanted token of type '%s', with value of '%s'. Got token of type '%s', with value of '%s'.", 
                    wantedType, wantedText, this.type, this.text),
                this.info
                )
            );
    }

    /// Compares two tokens to see if they're the same(ignores the debug info).
    // Too lazy to overload == and !=
    bool same(Token token)
    {
        return (this.text == token.text && this.type == token.type);
    }
}

/// Class to turn the engine's scripting language into tokens.
final class Lexer
{
    static assert(isInputRange!Lexer);

    private
    {//import std.stdio;
        DebugInfo   _currentInfo;

        // STuff related to the code
        string      _code;
        size_t      _index;

        // Range related stuff
        Token       _front;

        // Constant stuff
        immutable static char[]     _operators = ['>', ':', '$', '(', ')', ',', '!', '<', '>', '='];
        immutable static string[]   _keywords  = ["Room", "End", "Hook", "Call", "Else", "Include", "Script"];

        /// Skips blank space
        void skipBlanks()
        {
            while(!this.eof && this.peek.isWhite)
            {
                this.readChar!false;
            }
        }

        bool eof()
        {
            return (this._index >= this._code.length);
        }

        /// Reads in the next character, optionally parsing escape characters.
        char readChar(bool parseEscape = false)()
        {
            char next = this._code[this._index++];
            this._currentInfo.column += 1;

            if(next == '\n')
            {
                this._currentInfo.line += 1;
                this._currentInfo.column = 0;
            }

            static if(parseEscape)
            {
                if(next == '\\')
                {
                    this._enforce(!this.eof, "Unexpected EoF.");
                    next = this.readChar!false;

                    switch(next)
                    {
                        case 'n':
                            return '\n';
                        
                        case 't':
                            return '\t';

                        default:
                            this._enforce(false, "Unknown escape character: '%s'", next);
                            break;
                    }
                }
            }

            return next;
        }

        /// Reads in the next character, without moving forward in the code
        @property
        char peek()
        {
            return this._code[this._index];
        }

        /// Lexes and returns the next token.
        @property
        Token next()
        {
            this.skipBlanks();
            
            if(this.eof)
            {
                return Token(TokenType.EoF, "", this._currentInfo);
            }

            // See what the next character is
            auto next = this.peek;

            // Skip over comments
            while(true)
            {
                next = this.peek;
                if(next == '/')
                {
                    this.readChar();
                    if(this.peek == '/')
                    {
                        while(!this.eof && this.peek != '\n')
                        {
                            this.readChar();
                        }
                        if(!this.eof) this.readChar();

                        this.skipBlanks();
                    }
                    else
                    {
                        this._index -= 1;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            // Operators
            if(this._operators.canFind(next))
            {
                return Token(TokenType.Operator, [this.readChar!false], this._currentInfo);
            }
            else if(next.isDigit) // Numbers
            {
                size_t start = this._index;
                while(!this.eof && (this.peek.isDigit || this.peek == '.'))
                {
                    this._index += 1;
                }

                return Token(TokenType.Number, this._code[start..this._index], this._currentInfo);
            }
            else if(next == '"')// Strings
            {
                auto toReturn = Token(TokenType.String, "", this._currentInfo);
                this.readChar();

                while(true)
                {
                    this._enforce(!this.eof, "Unterminated string.");

                    next = this.readChar!true;
                    if(next == '"' && this._code[this._index - 1] != '\\')
                    {
                        return toReturn;
                    }
                    else
                    {
                        toReturn.text ~= next;
                    }
                }
            }
            else // Keywords + Identifiers
            {
                auto toReturn = Token(TokenType.Identifier, "", this._currentInfo);
                while(!this.eof && !this._operators.canFind(this.peek) && !this.peek.isWhite)
                {
                    toReturn.text ~= this.readChar!false;
                }

                if(this._keywords.canFind(toReturn.text))
                {
                    toReturn.type = TokenType.Keyword;
                }

                return toReturn;
            }
        }

        /// Convinience method for enforce.
        /// Function interface is the same as std.format's
        void _enforce(T, Args...)(T value, string _format, Args args)
        {
            enforce(value, new LexerException(format(_format, args), this._currentInfo));
        }
    }

    public
    {
        /// Construct a new Lexer, that will work on the given code.
        /// 
        /// Parameters:
        ///     code = The code to parse.
        ///     file = The file to insert into the token's debug info.
        this(string code, string file)
        {
            this._code              = code;
            this._currentInfo.file  = file;

            this.popFront();
        }

        /// Get the latest token
        @property
        Token front()
        {
            return this._front;
        }

        /// Lex the next token, and return the previous one.
        Token popFront()
        {
            auto old = this._front;
            this._front = this.next();

            return old;
        }

        /// Determines whether there's any more code to lex.
        @property
        bool empty()
        {
            return (this.front.type == TokenType.EoF);
        }
    }
}

/// Exception that is thrown when the lexer finds an issue
class LexerException : Exception
{
    /// Constructs a new LexerException.
    this(string message, DebugInfo info)
    {
        super(format("In file '%s'[%s:%s] %s", info.file, info.line, info.column, message));
    }
}

/// Exception that is thrown when something goes wrong with making the AST.
class ASTException : Exception
{
    /// Constructs a new ASTException.
    this(string message, DebugInfo info)
    {
        super(format("In file '%s'[%s:%s] %s", info.file, info.line, info.column, message));
    }
}

/// Base class for AST nodes
class ASTNode
{
    private
    {
        NodeType    _type;
    }

    public
    {
        /// Every inheriting class is forced to give a type
        this(NodeType type)
        {
            this._type = type;
        }

        /// Get what type of node this is.
        @property
        NodeType type()
        {
            return this._type;
        }

        /// Convert the node's data into a readable form.
        /// Use $(B indent) to tab out your data properly.
        abstract string asString(size_t indent = 0);

        override string toString()
        {
            return this.asString();
        }
    }

    protected
    {
        /// Convinience method for enforce.
        /// Function interface is the same as std.format's
        void _enforce(T, Args...)(T value, DebugInfo info, string _format, Args args)
        {
            enforce(value, new ASTException(format(_format, args), info));
        }

        /// Convinience function
        string indentFormat(Args...)(size_t indent, string _format, Args args)
        {
            char[] tabs;
            tabs.length = indent;
            tabs[] = '\t';

            return format("%s" ~ _format, tabs, args);
        }
    }
}

/// Node to describe some kind of declaration.
/// These nodes usually contain a bunch of other nodes.
class DeclarationNode : ASTNode
{
    private
    {
        string      _name;
        ASTNode[]   _children;
    }

    public
    {
        /// Construct a new Declaration node.
        /// 
        /// Parameters:
        ///     type = The type of declaration being made.
        ///     name = The name of the declaration.
        this(Token type, Token name)
        {
            // All of the declaration types are keywords.
            type.enforceType(TokenType.Keyword,    "Declaration type");
            name.enforceType(TokenType.Identifier, "Declaration name");

            this._name = name.text;
            NodeType finalType;
            switch(type.text)
            {
                case "Room":
                    finalType = NodeType.Decl_Room;
                    break;

                case "Hook":
                    finalType = NodeType.Decl_Hook;
                    break;

                case "Call":
                    finalType = NodeType.Decl_Call;
                    break;

                case "Script":
                    finalType = NodeType.Decl_Script;
                    break;

                default:
                    this._enforce(false, type.info, "Invalid declaration type: '%s'", type.text);
            }

            super(finalType);
        }

        /// Adds a node that is part of this declaration.
        /// 
        /// Parameters:
        ///     node = The node to add.
        void add(ASTNode node)
        {
            this._children ~= node;
        }

        override string asString(size_t indent = 0)
        {
            auto start = this.indentFormat(indent, "DeclarationNode[name='%s' Type='%s'] - Children:\n", this.name, this.type);
            foreach(child; this._children)
            {
                start ~= child.asString(indent + 1);
            }

            return start;
        }

        /// Get the declaration's name.
        @property
        string name()
        {
            return this._name;
        }

        /// Get this declaration's children.
        @property
        ASTNode[] children()
        {
            return this._children;
        }
    }
}

/// Node containing all of the nodes in a file.
/// Used to construct the overall AST of the code.
class FileNode : ASTNode
{
    private
    {//import std.stdio;
        Lexer       _lex;
        ASTNode[]   _nodes;

        /// Start making the AST
        void make()
        {
            auto include = Token(TokenType.Keyword, "Include");
            while(!this._lex.empty)
            {
                // Read in all of the different things
                if(this._lex.front.same(include))
                {
                    this.add(this.readInclude());
                }
                else
                {
                    this.add(this.readDeclaration());
                }
            }
        }

        /// Reads an include.
        IncludeNode readInclude()
        {
            this._lex.popFront().enforceAll(TokenType.Keyword, "Include");
            this._lex.popFront().enforceAll(TokenType.Operator, ":");

            return new IncludeNode(this._lex.popFront());
        }

        /// Reads in a declaration node(Doesn't read in call declarations)
        DeclarationNode readDeclaration()
        {
            //writeln("readDeclaration: ", this._lex.front);

            // Read in the type, and some syntax checking
            auto type = this._lex.popFront();
            this._lex.popFront().enforceAll(TokenType.Operator, ":");

            // Read in the name, and make the Declaration node. + A syntax check.
            auto node = new DeclarationNode(type, this._lex.popFront());
            foreach(nod; this.readBody(node.type))
            {
                node.add(nod);
            }

            return node;
        }

        /// Reads a body. Reads in different types of nodes depending on what $(B type) is.
        ASTNode[] readBody(NodeType type)
        {
            //writeln("readBody, type = ", type, ": ", this._lex.front);

            this._lex.popFront().enforceAll(TokenType.Operator, ">");
            ASTNode[] toReturn;

            // Read in the declaration.
            auto endToken = Token(TokenType.Keyword, "End");
            while(!this._lex.front.same(endToken))
            {
                // For rooms, just keep reading declarations
                if(type == NodeType.Decl_Room || type == NodeType.Decl_Script)
                {
                    toReturn ~= this.readDeclaration();
                }
                else if(type == NodeType.Decl_Hook) // For hooks, read in call statements.
                {
                    toReturn ~= this.readCall();
                }
                else
                {
                    assert(0);
                }
            }
            this._lex.popFront();

            return toReturn;
        }

        /// Reads in a call declaration.
        DeclarationNode readCall()
        {
            //writeln("readCall: ", this._lex.front);
            
            // Syntax check, and read the name in.
            this._lex.popFront().enforceAll(TokenType.Operator, "$");
            auto name = this._lex.popFront();
            this._lex.popFront().enforceAll(TokenType.Operator, "(");

            auto node = new DeclarationNode(Token(TokenType.Keyword, "Call", name.info), name);

            // Read in the parameters
            bool wantComma  = false;
            auto endBracket = Token(TokenType.Operator, ")");
            while(true)
            {
                auto next = this._lex.front;
                enforce(next.type != TokenType.EoF, new ASTException("Unexpectedly hit EoF.", next.info));

                // Break if we hit )
                if(next.same(endBracket))
                {
                    this._lex.popFront();
                    break;
                }

                // If we're looking for a comma, make sure there is one
                if(wantComma)
                {
                    wantComma = false;
                    this._lex.popFront().enforceAll(TokenType.Operator, ",");
                }
                else // Otherwise, read a parameter
                {
                    wantComma = true;
                    node.add(this.readBasicNode());
                }
            }

            // If this is an If call, then read in 2 more bodies.
            if(node.name == "If")
            {
                ValueNode makeDeclaration(ASTNode[] nodes)
                {
                    auto dec = new DeclarationNode(Token(TokenType.Keyword, "Hook"), Token(TokenType.Identifier, "IfCondition"));
                    foreach(node; nodes)
                    {
                        dec.add(node);
                    }

                    return new ValueNode(dec);
                }

                node.add(makeDeclaration(this.readBody(NodeType.Decl_Hook)));

                if(this._lex.front.same(Token(TokenType.Keyword, "Else")))
                {
                    this._lex.popFront().enforceAll(TokenType.Keyword, "Else");
                    node.add(makeDeclaration(this.readBody(NodeType.Decl_Hook)));
                }
            }

            return node;
        }

        /// This function can read in - Call nodes, and ValueNodes
        ValueNode readBasicNode()
        {
           //writeln("readBasicNode: ", this._lex.front);
            
            auto next = this._lex.front;

            // Do something depending on what the thing is.
            if(next.same(Token(TokenType.Operator, "$")))
            {
                return new ValueNode(this.readCall());
            }
            else if(next.type == TokenType.String || next.type == TokenType.Number)
            {
                this._lex.popFront();
                return new ValueNode(next);
            }

            this._enforce(false, next.info, "Invalid parameter. Token for parameter = %s", next);
            assert(false);
        }
    }

    public
    {
        /// Construct a file node that will then begin to fill itself out using the given lexer.
        /// 
        /// Parameters:
        ///     lex = The lexer to use.
        this(Lexer lex)
        {
            this._lex = lex;
            this.make();

            super(NodeType.File);
        }

        override string asString(size_t indent = 0)
        {
            auto start = this.indentFormat(indent, "FileNode[] - Children:\n");
            foreach(child; this._nodes)
            {
                start ~= child.asString(indent + 1);
            }
            
            return start;
        }

        /// Adds a node that is part of this declaration.
        /// 
        /// Parameters:
        ///     node = The node to add.
        void add(ASTNode node)
        {
            this._nodes ~= node;
        }
        
        /// Get this file's nodes.
        @property
        ASTNode[] children()
        {
            return this._nodes;
        }
    }
}

/// Node that contains an include.
class IncludeNode : ASTNode
{
    private
    {
        string _file;
    }

    public
    {
        /// Construct a new IncludeNode.
        /// 
        /// Parameters:
        ///     file = The file to include.
        this(Token file)
        {
            file.enforceType(TokenType.String, "Included file");
            this._file = file.text;

            super(NodeType.Include);
        }

        override string asString(size_t indent = 0)
        {
            return this.indentFormat(indent, "IncludeNode[file='%s']\n", this._file);
        }
        
        /// Get the file that this include wants.
        @property
        string file()
        {
            return this._file;
        }
    }
}

/// Node that contains a value
class ValueNode : ASTNode
{
    private
    {
        string  _string;
        size_t  _number;
        DeclarationNode _function;
    }

    public
    {
        /// Constructs a value node from the given token.
        this(Token value)
        {
            if(value.type != TokenType.String && value.type != TokenType.Number)
            {
                value.enforceType(TokenType.String, "StringOrNumber value");
            }

            if(value.type == TokenType.String)
            {
                super(NodeType.Value_String);
                this._string = value.text;
            }
            else
            {
                super(NodeType.Value_Number);
                this._number = value.text.to!(typeof(this._number));
                this._string = value.text;
            }
        }

        /// Constructs a value node that contains the given call/hook declaration
        this(DeclarationNode func)
        {
            enforce(func.type == NodeType.Decl_Call || func.type == NodeType.Decl_Hook, new ASTException(
                    format("Expected node of type 'Decl_Call' or 'Decl_Hook'. Got '%s'.", func.type), DebugInfo()));

            this._function = func;
            super((func.type == NodeType.Decl_Call) ? NodeType.Value_Call : NodeType.Value_Hook);
        }

        override string asString(size_t indent = 0)
        {
//            if(this.type != NodeType.Value_Hook)
//            {
                return this.indentFormat(indent, "ValueNode[string='%s' number=%s function='%s']\n", 
                    this._string, this._number, (this._function !is null) ? this._function.name : "N/A");
//            }
//            else
//            {
//                return this.indentFormat(indent, "ValueNode[string='%s' number=%s] - Hook:\n%s", 
//                    this._string, this._number, this._function.asString(indent + 1));
//            }
        }

        /// Get the string value of the ValueNode.
        @property
        string stringValue()
        {
            return this._string;
        }

        /// Get the number value of the ValueNode.
        @property
        size_t numberValue()
        {
            return this._number;
        }

        /// Get the function value of the ValueNode.
        @property
        DeclarationNode functionValue()
        {
            return this._function;
        }
    }
}

/// Functions that follow this interface can be called from a script.
alias ScriptHook = ValueNode[] delegate(ValueNode[] args);

/// The class that provides an enviroment for script code to run in.
final class ScriptEnvironment
{
    private
    {
        /// Contains the rooms of every script added.
        DeclarationNode[string] _rooms;

        /// Contains all of the variables set by the scripts.
        ValueNode[string]       _variables;

        /// Contains all custom-made hooks.
        ScriptHook[string]      _hooks;

        /// Contains all the hooks made in ScriptDeclarations.
        DeclarationNode[string] _scriptHooks;

        /// Contains stacks for the parameters.
        ValueNode[][string]     _stack;
    }

    public
    {
        /// Construct a new ScriptEnvironment, optionally passing it some variables.
        /// 
        /// Parameters:
        ///     variables = Variables to give the environment. E.G previous variables used in a past(but saved) session.
        this(ValueNode[string] variables = null)
        {
            if(variables !is null) this._variables = variables;

            this.addHook(&this.setVar,  "SetVar");
            this.addHook(&this.getVar,  "GetVar");
            this.addHook(&this.If,      "If");
        }

        /// Adds the script into the environment
        /// 
        /// Parameters:
        ///     node = The FileNode representing the script.
        void addScript(FileNode node)
        {
            foreach(child; node.children)
            {
                // Do different things, depending on what nodes there are.
                if(child.type == NodeType.Include)
                {
                    auto include = (cast(IncludeNode)child);
                    this.addScript(makeAST(include.file));
                }
                else if(child.type == NodeType.Decl_Room)
                {
                    // TODO: Make sure that the room name is unique
                    auto room = (cast(DeclarationNode)child);
                    this._rooms[room.name] = room;
                }
                else if(child.type == NodeType.Decl_Script)
                {
                    auto script = (cast(DeclarationNode)child);
                    foreach(hook; script.children)
                    {
                        enforce(hook.type == NodeType.Decl_Hook, new ScriptException(
                                format("The script '%s' contains an invalid node. They may only contain Hooks.", script.name)));
                                
                        auto _hook = cast(DeclarationNode)hook;
                        this._scriptHooks[_hook.name] = _hook;
                    }
                }
                else
                {
                    throw new ScriptException(
                        format("The given script contains an invalid node. A '%s' node.", child.type));
                }
            }
        }

        /// Adds a custom-made hook that the scripts can call using $(B name).
        /// 
        /// Parameters:
        ///     hook = The hook to add.
        ///     name = The name to give the hook.
        void addHook(ScriptHook hook, string name)
        {
            // TODO: Make sure the hook doesn't already exist.
            this._hooks[name] = hook;
        }

        /// Get the environment's variables.
        @property
        ValueNode[string] variables()
        {
            return this._variables;
        }
    }

    /// Built-in script functions
    public
    {
        /// Dispatches a hook depending on it's name.
        /// 
        /// Parameters:
        ///     name = The name of the hook to lookup.
        ///     args = The args to give the hook.
        ///     
        /// Returns:
        ///     Whatever.
        ValueNode[] dispatchHook(string name, ValueNode[] args)
        {            
            // Find which function to call
            auto pointer = (name in this._hooks);
            if(pointer !is null)
            {
                return (*pointer)(args);
            }
            else if((name in this._scriptHooks) is null)
            {
                throw new ScriptException(format("No hook with the name of '%s' exists.", name));
            }
            else
            {
                return this.executeHookSetParams(this._scriptHooks[name], args);
            }
        }

        /// Same thing as the other executeHook, except this one sets the ParamCount and related variables.
        ValueNode[] executeHookSetParams(DeclarationNode hook, ValueNode[] args)
        {
            // Setup the variables that explain the parameters, pushing the current ones onto a stack.
            string[] names;
            void pushVar(string name, ValueNode value)
            {
                auto pointer = (name in this._variables);
                if(pointer !is null)
                {
                    names ~= name;
                    
                    if((name in this._stack) is null)
                    {
                        this._stack[name] = [];
                    }
                    
                    this._stack[name] ~= *pointer;
                }
                
                this._variables[name] = value;
            }
            
            pushVar("ParamCount", new ValueNode(Token(TokenType.Number, args.length.to!string)));
            foreach(i, arg; args)
            {
                //import std.stdio; writeln("Setting ", i, " to ", arg);
                pushVar("Param"~((i+1).to!string), arg);
            }
            
            // Execute the hook
            this.executeHook(hook, args);
            
            // Then pop off everything
            foreach(namer; names)
            {
                this._variables[namer] = this._stack[namer].back;
                this._stack[namer].popBack();
            }
            
            return [];
        }
        
        /// Executes the given hook.
        /// 
        /// Parameters:
        ///     hook = The Hook to execute.
        ///     args = The parameters to give the hook.
        ///     
        /// Returns:
        ///     Whatever.
        ValueNode[] executeHook(DeclarationNode hook, ValueNode[] args)
        {
            assert(hook.type == NodeType.Decl_Hook);
            
            foreach(call; hook.children)
            {
                enforce(call.type == NodeType.Decl_Call, new ScriptException(
                        format("Hooks may only contain function calls. The Hook '%s' contains a '%s'", hook.name, call.type)));
                
                auto _call = cast(DeclarationNode)call;
                this.dispatchHook(_call.name, cast(ValueNode[])_call.children);
            }
            
            return [];
        }

        /// Executes the "main" function in the specified room.
        /// 
        /// Parameters:
        ///     room = The room to run.
        void runRoom(string room)
        {
            this.executeHook(this.getHookInRoom(room, "main"), []);
        }

        /// Finds a hook within the specified room.
        /// 
        /// Parmaeters:
        ///     Room = The room to check in.
        ///     hook = The hook to get.
        DeclarationNode getHookInRoom(string room, string hook)
        {
            auto pointer = (room in this._rooms);
            if(pointer !is null)
            {
                auto _room = *pointer;
                foreach(child; _room.children)
                {
                    auto _child = cast(DeclarationNode)child;
                    if(_child.type == NodeType.Decl_Hook && _child.name == hook)
                    {
                        return _child;
                    }
                }
            }
            else
            {
                // TODO: Exception
                assert(false);
            }
            assert(false);
        }

        /// Sets a variable to a value.
        /// 
        /// Parameters:
        ///     args[0] = Name of the variable. Must be a string
        ///     args[1] = Value of the variable.
        ///     
        /// Returns:
        ///     An empty array.
        ValueNode[] setVar(ValueNode[] args)
        {
            args = this.resolveParameters(args);

            this.enforceArgs(args.length, 2, "SetVar");
            this.enforceType(args[0], NodeType.Value_String, "SetVar");

            //import std.stdio; writeln(args[0].asString, args[1].asString);
            this._variables[args[0].stringValue] = args[1];

            return [];
        }

        /// Gets a variable.
        /// 
        /// Parameters:
        ///     args[0] = THe name of the variable to get. Must be a string
        ValueNode[] getVar(ValueNode[] args)
        {
            args = this.resolveParameters(args);
            this.enforceArgs(args.length, 1, "GetVar");
            this.enforceType(args[0], NodeType.Value_String, "GetVar");

            auto pointer = (args[0].stringValue in this._variables);
            if(pointer !is null)
            {
                //import std.stdio; writeln("\nGetVar:", args[0].asString, "=", *pointer);
                return [*pointer];
            }
            else
            {
                return [new ValueNode(Token(TokenType.String, "&VARIABLE_NOT_FOUND&"))];
            }
        }

        /// Performs an if statement.
        /// 
        /// Parameters:
        ///     arg[0] = Value1
        ///     arg[1] = Operation. Must be a string
        ///     arg[2] = Value2
        ///     arg[3] = ConditionTrueBody
        ///     arg[4] = Optional. ConditionFalseBody
        ValueNode[] If(ValueNode[] args)
        {
            args = args.dup;
            args[0..3] = this.resolveParameters(args[0..3]);

            // TODO: Length check.
            this.enforceType(args[1], NodeType.Value_String, "If");
            this.enforceType(args[3], NodeType.Value_Hook,   "If");
            if(args.length == 5) this.enforceType(args[4], NodeType.Value_Hook, "If");

            //import std.stdio;writeln(args);

            bool outcome = false;
            foreach(i, ch; args[1].stringValue)
            {
                if(i == 1 && ch == '=')
                {
                    // Numbers also have their string values made, so this is a good way to properly check the two.
                    outcome = (args[0].stringValue == args[2].stringValue);
                }
                else if(i == 0)
                {
                    if(ch == '>')
                    {
                        outcome = (args[0].stringValue > args[2].stringValue);
                    }
                    else if(ch == '<')
                    {
                        outcome = (args[0].stringValue < args[2].stringValue);
                    }
                    else if(ch == '=' || ch == '!') // Just so the if below this for doesn't break
                    {
                    }
                    else
                    {
                        // TODO: Exception
                        assert(0);
                    }

                    // If it's true by this point, then the next loop could actually make it false.
                    // So break early
                    if(outcome) break;
                }
                else if (i == 0)
                {
                    // TODO: Exception
                    assert(0);
                }
            }

            if(args[1].stringValue == "!=")
            {
                outcome = (args[0].stringValue != args[2].stringValue);
            }

            if(outcome)
            {
                this.executeHook(args[3].functionValue, []);
            }
            else if(args.length == 5)
            {
                this.executeHook(args[4].functionValue, []);
            }

            return [];
        }

        /// If any value in $(B params) is a function, it's ran with it's value returned.
        /// Otherwise, $(B param) is returned.
        ValueNode[] resolveParameters(ValueNode[] params)
        {
            ValueNode[] toReturn;

            foreach(param; params)
            {
                if(param.functionValue !is null)
                {
                    auto func = param.functionValue;
                    toReturn ~= this.dispatchHook(func.name, cast(ValueNode[])func.children);
                }
                else
                {
                    toReturn ~= param;
                }
            }

            return toReturn;
        }

        /// Convinience function to make sure that the right number of arguments were entered
        void enforceArgs(size_t got, size_t wanted, string func)
        {
            enforce(got == wanted, new ScriptException(
                    format("Invalid parameter count for function '%s'. Expected %s but got %s.", func, wanted, got)));
        }

        /// Convinience function to make sure a node has the right type.
        void enforceType(ASTNode got, NodeType wanted, string func)
        {
            enforce(got.type == wanted, new ScriptException(
                    format("Invalid parameter for function '%s'. Expected a '%s' but got a '%s'", func, wanted, got.type)));
        }
    }
}

// TODO: Go set things as pure, @nogc, const, inout, etc.
// TODO: Improve documentation
// TODO: Clean up the code a bit so If statements don't require a million special lines

/// The exception that is thrown when the SCript environment hits an issue
class ScriptException : Exception
{
    this(string message)
    {
        super(message);
    }
}

/// Creates an AST from the code in the file.
/// 
/// Parameters:
///     file = The file containing the code.
FileNode makeAST(string file)
{
    auto code = readText!string(file);
    auto lex  = new Lexer(code, file);

    return new FileNode(lex);
}