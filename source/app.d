import std.stdio, std.functional;

import engine.script;

ScriptEnvironment environment;

void main()
{
    environment         = new ScriptEnvironment();
    auto script         = makeAST("Debug/TestRooms.txt");
    environment.addScript(script);
    environment.addHook(toDelegate(&print), "Print");
    environment.runRoom("testRoom1");
    environment.executeHookSetParams(environment.getHookInRoom("testRoom1", "main"), [new ValueNode(Token(TokenType.String, "left"))]);

//    writeln(environment.variables);
//    writeln(script.asString);
}

ValueNode[] print(ValueNode[] args)
{
    args = environment.resolveParameters(args);

    foreach(arg; args)
    {
        write(arg.stringValue);
    }

    return [];
}